import React from 'react';
import {
  FlatList,
  Text,
  View,
  Animated,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

const y = new Animated.Value(0);
const onScroll = Animated.event([{ nativeEvent: { contentOffset: { y } } }], {
  useNativeDriver: true,
});

const CardHeight = 120;
const PosisionArray = [];
const isDisappearing = -CardHeight;
const isTop = 0;
const TranslateYArray = [];


class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      MyData: [],
      IsLoading: true,
    };
  }
  componentDidMount() {
    fetch('https://jsonplaceholder.typicode.com/users')
      .then((response) => response.json())
      .then((json) => {
        this.setState({ MyData: json });
      })
      .catch((error) => alert(error))
      .finally(() => {
        this.setState({ IsLoading: false });
      });
  }

  renderItem = ({ item, index }) => {

    PosisionArray.push(Animated.subtract(index * CardHeight, y))
    TranslateYArray.push(Animated.add(
      y,
      y.interpolate({
        inputRange: [0, 0.00001 + index * CardHeight],
        outputRange: [0, -index * CardHeight],
        extrapolateRight: "clamp",
      })
    ))

    var translateY = TranslateYArray[index]
    var position = PosisionArray[index]

    var scale = position.interpolate({
      inputRange: [isDisappearing, isTop],
      outputRange: [0.5, 1],
      extrapolate: "clamp",
    });


    return (
      <Animated.View
        style={[styles.CardStyle, { transform: [{ translateY }, { scale }] }]}
        key={index}>
        <Text style={styles.TextStyle}>ID: {item.id}</Text>
        <View
          style={{
            flexDirection: 'column',
            justifyContent: 'space-around'
          }}>
          <Text style={styles.TextStyle}>{item.username}</Text>

          <Text style={styles.TextStyle}>{item.name}</Text>
        </View>

      </Animated.View>
    )
  }

  render() {
    if (this.state.IsLoading) {
      return (<ActivityIndicator />)
    }
    return (
      <View style={{ backgroundColor: 'white', flex: 1, paddingTop: 15 }}>
        <AnimatedFlatList
          scrollEventThrottle={16}
          bounces={false}
          data={this.state.MyData}
          renderItem={this.renderItem}
          keyExtractor={(item) => item.index}
          {...{ onScroll }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  CardStyle: {
    flex: 1,
    flexDirection: 'row',
    padding: 10,
    backgroundColor: "white",
    marginHorizontal: 15,
    alignItems: 'center',
    justifyContent: 'space-around',
    marginBottom: 20,
    borderRadius: 15,
    height: CardHeight - 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.46,
    shadowRadius: 11.14,
    elevation: 10,
  },
  TextStyle: {
    textAlign: 'left',
    fontWeight: 'bold',
    fontSize: 17,
  }
})
export default App;